```
    ---.
   -..  \
     _|_|_
    /  O    \
    \_______/
       /   \
       \/   \
       /'---'\
  ____/  |     \_____
       __/\____/      \_
            |            \
          / \__  /\      '_
          /     \__ \        \
          \        \_\_________\
           \          \     \
            \          \
```     
# Documents		
## AES
- https://stackoverflow.com/questions/273452/using-aes-encryption-in-c-sharp
## RSA
- https://stackoverflow.com/questions/17128038/c-sharp-rsa-encryption-decryption-with-transmission
## ECC
- https://www.codeproject.com/Tips/1071190/Encryption-and-Decryption-of-Data-using-Elliptic-C
- https://gist.github.com/tmarkovski/9fc008fc034511bbbee93a5c4cd1a99a
### Encryption
- Define a Curve
- Generate public private Key pair using that curve, for both sender and receiver
- Generate a Shared secret key from the key pair
- From that shared secret key, generate an encryption key
- Using that encryption key and symmetric encryption algorithm, encrypt the data to send
### Decryption
- The sender will either share the curve with receiver or sender and receiver will have the same use for the same curve type. Also, sender will share its public key with receiver.
- Generate public private Key pair using the same curve for that curve. For receiver.
- Regenerate a shared secret key using private key of receiver and public key of sender.
- From that shared secret key, generate an encryption key
	-Using that encryption key and symmetric encryption algorithm, decrypt the data
