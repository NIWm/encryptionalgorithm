﻿using EncryptionAlgorithm.Helper;
using EncryptionAlgorithm.Models;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Asn1.Anssi;
using Org.BouncyCastle.Asn1.Nist;
using Org.BouncyCastle.Asn1.TeleTrust;
using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math.EC;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EncryptionAlgorithm.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            var str = "DOANVANQUYNHAT13IU";
            byte[] strBytes = ASCIIEncoding.ASCII.GetBytes(str);
            #region AES 
            var cypherText = AES.Encrypt(str, "AT13IU");
            var plainText = AES.Decrypt(cypherText, "AT13IU");
            #endregion

            #region RSA

            RSAHelper.Init(RSAType.RSA2, Encoding.UTF8);

            var enStr = RSAHelper.Encrypt(str);

            var deStr = RSAHelper.Decrypt(enStr);

            var signStr = RSAHelper.Sign(str);

            var signVerify = RSAHelper.Verify(str, signStr);
            #endregion

            #region ECC
            var senderKeypair = ECC.GenerateKeyPair();
          
            var sharedSecretKey = ECC.GetSharedSecretValue((ECPrivateKeyParameters)senderKeypair.Private, (ECPublicKeyParameters)senderKeypair.Public);
            var encryptionKey = ECC.DeriveSymmetricKeyFromSharedSecret(sharedSecretKey);
            var cipherBytes = ECC.Encrypt(strBytes, encryptionKey);
         

            //-----------------------Receiver-------------------------------
            var receiverKeypair = ECC.GenerateKeyPair();

            var sharedReceiverSecretKey = ECC.GetSharedSecretValue((ECPrivateKeyParameters)receiverKeypair.Private, (ECPublicKeyParameters)senderKeypair.Public);

            var encryptionReceiverKey = ECC.DeriveSymmetricKeyFromSharedSecret(sharedReceiverSecretKey);
            var plainTextBytes = ECC.Decrypt(cipherBytes, encryptionKey);

            var plText = Encoding.UTF8.GetString(plainTextBytes, 0, plainTextBytes.Length);

            #endregion
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

          }
}
